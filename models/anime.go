package models

import (
	"fmt"
	"github/epith/302-backend-user/bob"
	"net/http"
)

type UpdateAnime struct {
	*bob.Anime
}

func (anime *UpdateAnime) Bind(r *http.Request) error {
	if anime.Anime == nil {
		return fmt.Errorf("missing required Anime fields")
	}
	return nil
}
