package handler

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	bob "github/epith/302-backend-user/bob"
	"github/epith/302-backend-user/bob/factory"
	"github/epith/302-backend-user/db"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/aarondl/opt/null"
	_ "github.com/lib/pq"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	log "github.com/sirupsen/logrus"
	bobby "github.com/stephenafamo/bob"
	"gotest.tools/v3/assert"
)

var db_test db.Database

type Response struct {
	Message string `json:"message"`
	Service string `json:"service"`
}

const tableCreationQuery = `
DROP TABLE IF EXISTS anime;
DROP TABLE IF EXISTS genre;
CREATE TABLE IF NOT EXISTS anime 
(
	id serial,
    romaji character varying null,
    english character varying null,
    native character varying null,
    format character varying null,
    status character varying null,
    description character varying null,
    "startYear" integer null,
    "startMonth" integer null,
    "startDay" integer null,
    "endYear" integer null,
    "endMonth" integer null,
    "endDay" integer null,
    season character varying null,
    "seasonYear" character varying null,
    episodes character varying null,
    duration character varying null,
    "countryOfOrigin" character varying null,
    "updatedAt" integer null,
    "coverImage" character varying null,
    "averageScore" integer null,
    popularity integer null,
    "AiringAt" integer null,
    "nextAiringEpisode" integer null,
    constraint anime_pkey primary key (id)
  ) tablespace pg_default;
CREATE TABLE genre (
	id serial4 NOT NULL,
	"name" varchar NULL,
	anime_id int4 NULL,
	CONSTRAINT genre_pkey PRIMARY KEY (id)
);
`

func setupTest(tb testing.TB) func(tb testing.TB) {
	//insert 3 users with username: Epith, Omurice, Yum Yum Banana
	insertAnimes()
	// Return a function to teardown the test
	return func(tb testing.TB) {
		clearTable()
	}
}

func ensureTableExists() {
	if _, err := db_test.Conn.ExecContext(context.Background(), tableCreationQuery); err != nil {
		log.Fatal(err)
	}
}

func clearTable() {
	if _, err := db_test.Conn.ExecContext(context.Background(), "DELETE FROM anime"); err != nil {
		log.Fatal(err)
	}
	if _, err := db_test.Conn.ExecContext(context.Background(), "DELETE FROM genre"); err != nil {
		log.Fatal(err)
	}
}

func insertAnimes() {
	f := factory.New()
	animeTemplate1 := f.NewAnime(
		factory.AnimeMods.ID(21),
		factory.AnimeMods.English(null.From("ONE PIECE")),
		factory.AnimeMods.Format(null.From("TV")),
		factory.AnimeMods.Status(null.From("RELEASING")),
		factory.AnimeMods.Popularity(null.From(100)),
	)

	animeTemplate2 := f.NewAnime(
		factory.AnimeMods.ID(51),
		factory.AnimeMods.English(null.From("Dragon Ball Z")),
		factory.AnimeMods.Format(null.From("TV")),
		factory.AnimeMods.Status(null.From("RELEASING")),
		factory.AnimeMods.Popularity(null.From(90)),
	)

	animeTemplate3 := f.NewAnime(
		factory.AnimeMods.ID(1),
		factory.AnimeMods.English(null.From("Demon Slayer")),
		factory.AnimeMods.Format(null.From("TV")),
		factory.AnimeMods.Status(null.From("RELEASING")),
		factory.AnimeMods.Popularity(null.From(95)),
	)

	genreTemplate1 := f.NewGenre(factory.GenreMods.Name(null.From("Comedy")), factory.GenreMods.AnimeID(null.From(21)))

	animeTemplate1.Create(context.Background(), db_test.Conn)
	animeTemplate2.Create(context.Background(), db_test.Conn)
	animeTemplate3.Create(context.Background(), db_test.Conn)
	genreTemplate1.Create(context.Background(), db_test.Conn)
}

func TestMain(m *testing.M) {

	// uses a sensible default on windows (tcp/http) and linux/osx (socket)
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not construct pool: %s", err)
	}

	err = pool.Client.Ping()
	if err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}

	// pulls an image, creates a container based on it and runs it
	resource, err := pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "postgres",
		Tag:        "11",
		Env: []string{
			"POSTGRES_PASSWORD=secret",
			"POSTGRES_USER=user_name",
			"POSTGRES_DB=dbname",
			"listen_addresses = '*'",
		},
	}, func(config *docker.HostConfig) {
		// set AutoRemove to true so that stopped container goes away by itself
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{Name: "no"}
	})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	port := resource.GetPort("5432/tcp")
	databaseUrl := fmt.Sprintf("postgres://user_name:secret@docker:%s/dbname?sslmode=disable", port)

	log.Println("Connecting to database on url: ", databaseUrl)

	resource.Expire(120) // Tell docker to hard kill the container in 120 seconds

	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	pool.MaxWait = 120 * time.Second
	if err = pool.Retry(func() error {
		db_test.Conn, err = bobby.Open("postgres", databaseUrl)
		if err != nil {
			return err
		}
		return db_test.Conn.PingContext(context.Background())
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	ensureTableExists()
	//Run tests
	code := m.Run()
	// You can't defer this because os.Exit doesn't care for defer
	if err := pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}

func TestGet_Health(t *testing.T) {
	//setting up the request and handler to get the response back
	httpHandler := NewHandler(db_test)
	req, err := http.NewRequest(http.MethodGet, "/health", nil)
	if err != nil {
		log.Fatal(err)
	}
	rr := httptest.NewRecorder()
	httpHandler.ServeHTTP(rr, req)

	//unmarshal the response
	healthResponse := Response{}
	err = json.Unmarshal(rr.Body.Bytes(), &healthResponse)
	if err != nil {
		log.Fatal(err)
	}
	assert.Equal(t, rr.Code, http.StatusOK)
	assert.Equal(t, healthResponse.Message, "Service is healthy.")
	assert.Equal(t, healthResponse.Service, "anime")
}
func TestGet_AnimesEmpty(t *testing.T) {
	//setting up the request and handler to get the response back
	httpHandler := NewHandler(db_test)
	req, err := http.NewRequest(http.MethodGet, "/anime", nil)
	if err != nil {
		log.Fatal(err)
	}
	rr := httptest.NewRecorder()
	httpHandler.ServeHTTP(rr, req)

	//unmarshal the response
	animes := bob.AnimeSlice{}
	err = json.Unmarshal(rr.Body.Bytes(), &animes)
	if err != nil {
		log.Fatal(err)
	}
	assert.Equal(t, rr.Code, http.StatusOK)
	assert.Equal(t, len(animes), 0)
}

func TestGet_AllAnime(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	//setting up the request and handler to get the response back
	httpHandler := NewHandler(db_test)
	req, err := http.NewRequest(http.MethodGet, "/anime", nil)
	if err != nil {
		log.Fatal(err)
	}
	rr := httptest.NewRecorder()
	httpHandler.ServeHTTP(rr, req)

	//unmarshal the response
	animes := make([]AnimeWithGenre, 0)
	err = json.Unmarshal(rr.Body.Bytes(), &animes)
	if err != nil {
		log.Fatal(err)
	}

	animeName1, _ := animes[0].Anime.English.Get()
	animeName2, _ := animes[1].Anime.English.Get()
	animeName3, _ := animes[2].Anime.English.Get()

	assert.Equal(t, len(animes), 3)
	assert.Equal(t, *&animes[0].Anime.ID, 21)
	assert.Equal(t, *&animes[1].Anime.ID, 51)
	assert.Equal(t, *&animes[2].Anime.ID, 1)
	assert.Equal(t, animeName1, "ONE PIECE")
	assert.Equal(t, animeName2, "Dragon Ball Z")
	assert.Equal(t, animeName3, "Demon Slayer")
}

func TestGet_SingleAnime(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	//setting up the request and handler to get the response back
	httpHandler := NewHandler(db_test)
	req, err := http.NewRequest(http.MethodGet, "/anime/21", nil)
	if err != nil {
		log.Fatal(err)
	}
	rr := httptest.NewRecorder()
	httpHandler.ServeHTTP(rr, req)

	//unmarshal the response
	anime := AnimeWithGenre{}
	err = json.Unmarshal(rr.Body.Bytes(), &anime)
	if err != nil {
		log.Fatal(err)
	}

	animeName1, _ := anime.Anime.English.Get()

	assert.Equal(t, anime.Anime.ID, 21)
	assert.Equal(t, animeName1, "ONE PIECE")
}

func TestGet_SingleInvalidAnime(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	//setting up the request and handler to get the response back
	httpHandler := NewHandler(db_test)
	req, err := http.NewRequest(http.MethodGet, "/anime/99", nil)
	if err != nil {
		log.Fatal(err)
	}
	rr := httptest.NewRecorder()
	httpHandler.ServeHTTP(rr, req)

	//unmarshal the response
	errorMsg := ErrorResponse{}
	err = json.Unmarshal(rr.Body.Bytes(), &errorMsg)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(errorMsg)
	assert.Equal(t, errorMsg.StatusText, "Bad request")
	assert.Equal(t, rr.Code, 400)
}

func TestGet_MostPopularAnime(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	//setting up the request and handler to get the response back
	httpHandler := NewHandler(db_test)
	req, err := http.NewRequest(http.MethodGet, "/anime/popularity/3", nil)
	if err != nil {
		log.Fatal(err)
	}
	rr := httptest.NewRecorder()
	httpHandler.ServeHTTP(rr, req)

	//unmarshal the response
	animes := bob.AnimeSlice{}
	err = json.Unmarshal(rr.Body.Bytes(), &animes)
	if err != nil {
		log.Fatal(err)
	}

	animeName1, _ := animes[0].English.Get()
	animeName2, _ := animes[1].English.Get()
	animeName3, _ := animes[2].English.Get()

	assert.Equal(t, len(animes), 3)
	assert.Equal(t, *&animes[0].ID, 21)
	assert.Equal(t, *&animes[1].ID, 1)
	assert.Equal(t, *&animes[2].ID, 51)
	assert.Equal(t, animeName1, "ONE PIECE")
	assert.Equal(t, animeName2, "Demon Slayer")
	assert.Equal(t, animeName3, "Dragon Ball Z")
}

func TestGet_AnimeByGenre(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	//setting up the request and handler to get the response back
	httpHandler := NewHandler(db_test)
	req, err := http.NewRequest(http.MethodGet, "/anime/genre?genre=Comedy&n=5", nil)
	if err != nil {
		log.Fatal(err)
	}
	rr := httptest.NewRecorder()
	httpHandler.ServeHTTP(rr, req)

	//unmarshal the response
	genres := bob.GenreSlice{}
	err = json.Unmarshal(rr.Body.Bytes(), &genres)
	if err != nil {
		log.Fatal(err)
	}

	animeGenre1, _ := genres[0].Name.Get()
	animeId1, _ := genres[0].AnimeID.Get()

	assert.Equal(t, len(genres), 1)
	assert.Equal(t, animeGenre1, "Comedy")
	assert.Equal(t, animeId1, 21)

}

func TestPost(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	//setting up the request and handler to get the response back
	httpHandler := NewHandler(db_test)
	rr := httptest.NewRecorder()
	jsonBody := []byte(`{"Id": "21"}`)
	bodyReader := bytes.NewReader(jsonBody)
	req, err := http.NewRequest(http.MethodPost, "/anime", bodyReader)
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		log.Fatal(err)
	}
	httpHandler.ServeHTTP(rr, req)

	assert.Equal(t, rr.Code, 405)
}

func TestDelete_ValidAnime(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	//setting up the request and handler to get the response back
	httpHandler := NewHandler(db_test)
	rr := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodDelete, "/anime/21", nil)
	if err != nil {
		log.Fatal(err)
	}
	httpHandler.ServeHTTP(rr, req)
	//unmarshal the response
	assert.Equal(t, rr.Code, 200)
}

func TestDelete_InvalidAnime(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	//setting up the request and handler to get the response back
	httpHandler := NewHandler(db_test)
	rr := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodDelete, "/anime/33", nil)
	if err != nil {
		log.Fatal(err)
	}
	httpHandler.ServeHTTP(rr, req)
	//unmarshal the response
	errorMsg := ErrorResponse{}
	err = json.Unmarshal(rr.Body.Bytes(), &errorMsg)
	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, rr.Code, 500)
	assert.Equal(t, errorMsg.Message, "anime does not exist")
}
