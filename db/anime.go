package db

import (
	"context"
	"errors"
	"fmt"
	"github/epith/302-backend-user/bob"
	"log"
	"strings"

	"github.com/aarondl/opt/omitnull"
	"github.com/stephenafamo/bob/dialect/psql"
	"github.com/stephenafamo/bob/dialect/psql/sm"
)

func (db Database) GetAllAnime() (bob.AnimeSlice, error) {
	ctx := context.Background()

	tx, err := db.Conn.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := tx.Rollback(); err != nil {
			log.Println(err)
		}
	}()

	rows, err := bob.Animes.Query(ctx, tx).All()
	if err != nil {
		return nil, err
	}
	err = rows.LoadAnimeGenres(ctx, tx)
	if err != nil {
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		return nil, err
	}

	return rows, nil
}

func (db Database) GetAnimeByID(id int) (*bob.Anime, error) {
	ctx := context.Background()
	tx, err := db.Conn.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	anime, err := bob.FindAnime(ctx, tx, id)
	if err != nil {
		return nil, err
	}
	err = anime.LoadAnimeGenres(ctx, tx)
	if err != nil {
		return nil, err
	}
	if err := tx.Commit(); err != nil {
		return nil, err
	}
	return anime, err
}

func (db Database) GetAnimesByID(id int) (*bob.Anime, error) {
	ctx := context.Background()
	anime, err := bob.FindAnime(ctx, db.Conn, id)

	return anime, err
}

func (db Database) UpdateAnime(id int, animeData *bob.Anime) (*bob.Anime, error) {
	ctx := context.Background()
	tx, err := db.Conn.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := tx.Rollback(); err != nil {
			log.Println(err)
		}
	}()

	anime, err := bob.FindAnime(ctx, tx, id)
	if err != nil {
		return nil, err
	}
	var updateAnime = &bob.AnimeSetter{}

	// End Time
	if !anime.EndYear.IsNull() {
		updateAnime.EndYear = omitnull.FromNull(animeData.EndYear)
	}
	if !anime.EndMonth.IsNull() {
		updateAnime.EndMonth = omitnull.FromNull(animeData.EndMonth)
	}
	if !anime.EndDay.IsNull() {
		updateAnime.EndDay = omitnull.FromNull(animeData.EndDay)
	}

	// Anime Info
	if !anime.Episodes.IsNull() {
		updateAnime.Episodes = omitnull.FromNull(animeData.Episodes)
	}
	if !anime.AverageScore.IsNull() {
		updateAnime.AverageScore = omitnull.FromNull(animeData.AverageScore)
	}
	if !anime.Popularity.IsNull() {
		updateAnime.Popularity = omitnull.FromNull(animeData.Popularity)
	}

	// Schedule
	if !anime.UpdatedAt.IsNull() {
		updateAnime.UpdatedAt = omitnull.FromNull(animeData.UpdatedAt)
	}
	if !anime.AiringAt.IsNull() {
		updateAnime.AiringAt = omitnull.FromNull(animeData.AiringAt)
	}
	if !anime.NextAiringEpisode.IsNull() {
		updateAnime.NextAiringEpisode = omitnull.FromNull(animeData.NextAiringEpisode)
	}

	// Try to update
	err = anime.Update(ctx, tx, updateAnime)

	// Commit the transactions
	if err := tx.Commit(); err != nil {
		return nil, err
	}

	return anime, err
}

func (db Database) DeleteAnime(id int) error {
	ctx := context.Background()
	tx, err := db.Conn.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer func() {
		if err := tx.Rollback(); err != nil {
			log.Println(err)
		}
	}()
	anime, err := bob.FindAnime(ctx, tx, id)
	if err != nil {
		return errors.New("anime does not exist")
	}
	err = anime.Delete(ctx, tx)
	if err := tx.Commit(); err != nil {
		return err
	}
	return err
}

func (db Database) GetMostPopularAnime(n int) (bob.AnimeSlice, error) {
	ctx := context.Background()
	rows, err := bob.Animes.Query(ctx, db.Conn, sm.OrderBy("popularity Desc"), sm.Limit(n)).All()
	if err != nil {
		return rows, err
	}

	return rows, nil
}

func (db Database) GetAllAnimeByGenre(animeGenre string) (bob.GenreSlice, error) {
	ctx := context.Background()
	rows, err := bob.Genres.Query(ctx, db.Conn, sm.Where(psql.Quote("name").In(psql.Arg(animeGenre)))).All()
	if err != nil {
		return rows, err
	}
	return rows, nil
}

func (db Database) GetRecommendations(animeIDs []int) (bob.AnimeSlice, error) {
	ctx := context.Background()
	inValues := make([]string, len(animeIDs))
	for i, v := range animeIDs {
		inValues[i] = fmt.Sprintf("%d", v)
	}
	inClause := strings.Join(inValues, ",")
	rows, err := bob.Animes.Query(ctx, db.Conn, sm.Where(psql.Raw("id IN ("+inClause+")")), sm.OrderBy("popularity Desc")).All()
	if err != nil {
		return rows, err
	}
	return rows, nil
}
