package db

import (
	"context"
	"fmt"
	"github/epith/302-backend-user/bob/factory"
	"os"
	"testing"
	"time"

	"github.com/aarondl/opt/null"
	_ "github.com/lib/pq"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	log "github.com/sirupsen/logrus"
	"github.com/stephenafamo/bob"
	"gotest.tools/v3/assert"
)

var db_test Database

func TestMain(m *testing.M) {
	// uses a sensible default on windows (tcp/http) and linux/osx (socket)
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not construct pool: %s", err)
	}

	err = pool.Client.Ping()
	if err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}

	// pulls an image, creates a container based on it and runs it
	resource, err := pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "postgres",
		Tag:        "11",
		Env: []string{
			"POSTGRES_PASSWORD=secret",
			"POSTGRES_USER=user_name",
			"POSTGRES_DB=dbname",
			"listen_addresses = '*'",
		},
	}, func(config *docker.HostConfig) {
		// set AutoRemove to true so that stopped container goes away by itself
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{Name: "no"}
	})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	port := resource.GetPort("5432/tcp")
	databaseUrl := fmt.Sprintf("postgres://user_name:secret@docker:%s/dbname?sslmode=disable", port)

	log.Println("Connecting to database on url: ", databaseUrl)

	resource.Expire(120) // Tell docker to hard kill the container in 120 seconds

	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	pool.MaxWait = 120 * time.Second
	if err = pool.Retry(func() error {
		db_test.Conn, err = bob.Open("postgres", databaseUrl)
		if err != nil {
			return err
		}
		return db_test.Conn.PingContext(context.Background())
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	ensureTableExists()
	//Run tests
	code := m.Run()
	// You can't defer this because os.Exit doesn't care for defer
	if err := pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}

func TestGetAnimeEmpty(t *testing.T) {
	anime, err := db_test.GetAllAnime()
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, len(anime), 0)
}

func TestGetAllAnime(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	anime, err := db_test.GetAllAnime()
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, len(anime), 3)
	assert.Equal(t, *&anime[0].ID, 1)
	assert.Equal(t, *&anime[1].ID, 2)
	assert.Equal(t, *&anime[2].ID, 3)
}

func TestGetOneAnime(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	anime, err := db_test.GetAnimeByID(1)
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, *&anime.ID, 1)
}

// not really user side, but ill leave it here first
// func TestUpdateAnime(t *testing.T) {
// 	teardownTest := setupTest(t)
// 	defer teardownTest(t)

// 	anime2, err := db_test.GetAnimeByID(1)
// 	if err != nil {
// 		log.Println(err)
// 	}

// 	anime2.Status.Set("Cancelled")
// 	anime, err := db_test.UpdateAnime(1, &anime2)
// 	if err != nil {
// 		log.Println(err)
// 	}
// 	assert.Equal(t, anime.Status, "Cancelled")
// }

func TestDeleteAnime(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	err := db_test.DeleteAnime(3)
	if err != nil {
		log.Println(err)
	}
}

func TestGetMostPopularAnime(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	popularAnime, err := db_test.GetMostPopularAnime(2)
	if err != nil {
		log.Println(err)
	}

	assert.Equal(t, *&popularAnime[0].ID, 3)
	assert.Equal(t, *&popularAnime[1].ID, 2)
}

// deal with it later
// func TestGetNAnimeByGenre(t *testing.T) {
// 	teardownTest := setupTest(t)
// 	defer teardownTest(t)

// }

func setupTest(tb testing.TB) func(tb testing.TB) {
	//insert 3 anime with id: 1, 2, 3
	insertAnime()
	// Return a function to teardown the test
	return func(tb testing.TB) {
		clearTable()
	}
}

func dropTable() {
	if _, err := db_test.Conn.ExecContext(context.Background(), "DROP TABLE IF EXISTS genre"); err != nil {
		log.Fatal(err)
	}
	if _, err := db_test.Conn.ExecContext(context.Background(), "DROP TABLE IF EXISTS anime"); err != nil {
		log.Fatal(err)
	}
}

func ensureTableExists() {
	if _, err := db_test.Conn.ExecContext(context.Background(), tableCreationQuery); err != nil {
		log.Fatal(err)
	}
}

func clearTable() {
	if _, err := db_test.Conn.ExecContext(context.Background(), "DELETE FROM anime"); err != nil {
		log.Fatal(err)
	}
	if _, err := db_test.Conn.ExecContext(context.Background(), "DELETE FROM genre"); err != nil {
		log.Fatal(err)
	}
}

func insertAnime() {
	f := factory.New()
	animeTemplate1 := f.NewAnime(factory.AnimeMods.ID(1), factory.AnimeMods.Popularity(null.From(10)), factory.AnimeMods.AddNewGenres(1))
	animeTemplate2 := f.NewAnime(factory.AnimeMods.ID(2), factory.AnimeMods.Popularity(null.From(50)), factory.AnimeMods.AddNewGenres(2))
	animeTemplate3 := f.NewAnime(factory.AnimeMods.ID(3), factory.AnimeMods.Popularity(null.From(100)), factory.AnimeMods.AddNewGenres(3))

	animeTemplate1.Create(context.Background(), db_test.Conn)
	animeTemplate2.Create(context.Background(), db_test.Conn)
	animeTemplate3.Create(context.Background(), db_test.Conn)
}

const tableCreationQuery = `
CREATE EXTENSION pgcrypto;
CREATE TABLE IF NOT EXISTS anime 
(
	id serial,
    romaji character varying null,
    english character varying null,
    native character varying null,
    format character varying null,
    status character varying null,
    description character varying null,
    "startYear" integer null,
    "startMonth" integer null,
    "startDay" integer null,
    "endYear" integer null,
    "endMonth" integer null,
    "endDay" integer null,
    season character varying null,
    "seasonYear" character varying null,
    episodes character varying null,
    duration character varying null,
    "countryOfOrigin" character varying null,
    "updatedAt" integer null,
    "coverImage" character varying null,
    "averageScore" integer null,
    popularity integer null,
    "AiringAt" integer null,
    "nextAiringEpisode" integer null,
    constraint anime_pkey primary key (id)
  ) tablespace pg_default;
  CREATE TABLE genre (
	id serial4 NOT NULL,
	"name" varchar NULL,
	anime_id int4 NULL,
	CONSTRAINT genre_pkey PRIMARY KEY (id)
);
`
